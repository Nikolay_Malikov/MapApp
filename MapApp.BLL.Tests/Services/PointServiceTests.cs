﻿using System;
using System.Collections.Generic;
using MapApp.BLL.Contracts;
using MapApp.BLL.Services;
using MapApp.DAL.Repository.Point;
using MapApp.DATA.Model;
using MapApp.Tests.Common;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace MapApp.BLL.Tests.Services
{
    [TestClass]
    public class PointServiceTests: UnityContainerTests
    {
        private readonly Mock<IPointRepository> _pointRepository = new Mock<IPointRepository>();

        [TestInitialize]
        public override void InitializeTests()
        {
            UnityContainer = new UnityContainer()
                .RegisterInstance(typeof(IPointRepository), _pointRepository.Object)
                .RegisterType<IPointService, PointService>();
                
            ServiceLocator = new UnityServiceLocator(UnityContainer);
        }

        [TestMethod]
        public void GetPoints()
        {
            var pointList = new List<Point>();
            _pointRepository.Setup(
                    x => x.GetPoints(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(pointList)
                .Verifiable("Method should be called");
            var service = ServiceLocator.GetInstance(typeof(IPointService)) as IPointService;

            //Act
            var result = service.GetPoints("","");

            //Assert
            Assert.AreEqual(result.Count, pointList.Count);
            _pointRepository.Verify();
        }
    }
}
