﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MapApp.DATA.Model;

namespace MapApp.BLL.Contracts
{
    public interface IStateService
    {
        IList<State> GetStates();
    }
}
