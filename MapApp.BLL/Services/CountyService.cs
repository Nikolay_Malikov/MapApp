﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MapApp.BLL.Contracts;
using MapApp.DAL.Repository.City;
using MapApp.DATA.Model;

namespace MapApp.BLL.Services
{
    public class CountyService: ICountyService
    {
        private readonly ICountyRepository _cityRepository;
        public CountyService(ICountyRepository countyRepository)
        {
            _cityRepository = countyRepository;
        }

        public IList<County> GetCities(string state)
        {
            return _cityRepository.GetCounties(state);
        }
    }
}
