﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MapApp.BLL.Contracts;
using MapApp.DAL.Repository.Point;
using MapApp.DATA.Model;

namespace MapApp.BLL.Services
{
    public class PointService: IPointService
    {
        private readonly IPointRepository _pointRepository;
        public PointService(IPointRepository pointRepository)
        {
            _pointRepository = pointRepository;
        }

        public IList<Point> GetPoints(string county, string state)
        {
            return _pointRepository.GetPoints(state, county);
        }
    }
}
