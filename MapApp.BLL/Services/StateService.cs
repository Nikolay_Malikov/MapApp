﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MapApp.BLL.Contracts;
using MapApp.DAL.Repository.State;
using MapApp.DATA.Model;

namespace MapApp.BLL.Services
{
    public class StateService:IStateService
    {
        private readonly IStateRepository _stateRepository;
        public StateService(IStateRepository stateRepository)
        {
            _stateRepository = stateRepository;
        }
        public IList<State> GetStates()
        {
            return _stateRepository.GetAll();
        }
    }
}
