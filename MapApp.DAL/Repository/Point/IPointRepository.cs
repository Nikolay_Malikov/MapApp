﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapApp.DAL.Repository.Point
{
    public interface IPointRepository
    {
        IList<DATA.Model.Point> GetPoints(string state, string county);
    }
}
