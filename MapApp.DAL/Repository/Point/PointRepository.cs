﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using MapApp.DATA.Model;

namespace MapApp.DAL.Repository.Point
{
    public class PointRepository:IPointRepository
    {
        public IList<DATA.Model.Point> GetPoints(string state, string county)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
               return db.Query<DATA.Model.Point>("Select cml.[Property City] as City, cml.[Property Full Address] as FullAddress, cml.[Property State] as State, cml.[Mechanic Tax Lien Document Number]  as LienNumber, cml.[Owner Full Name] as Owner, cml.[Mechanics Lien Holder Name 1] as LienHolder, SUBSTRING(cml.[Property Zip Code +4], 0, 6) as Zip, cml.LATTITUDE as lat, cml.LONGITUDE as lng FROM CommercialC AS cml WHERE cml.LATTITUDE is not null and cml.LONGITUDE is not null AND (cml.[Property State] = @state or @state = '') AND (cml.[Property County Name] = @county or @county = '')", new { state, county }).ToList();
            }
        }
    }
}
