﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapApp.DAL.Repository.State
{
    public interface IStateRepository
    {
        IList<DATA.Model.State> GetAll();
    }
}
