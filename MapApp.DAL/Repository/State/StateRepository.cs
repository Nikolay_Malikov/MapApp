﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using MapApp.DATA.Model;

namespace MapApp.DAL.Repository.State
{
    public class StateRepository:IStateRepository
    {
        public IList<DATA.Model.State> GetAll()
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                return db.Query<DATA.Model.State>("Select DISTINCT(cml.[Property State]) as Name FROM Commercial AS cml WHERE cml.[Property State] IS NOT null").ToList();
            }
        }
    }
}
