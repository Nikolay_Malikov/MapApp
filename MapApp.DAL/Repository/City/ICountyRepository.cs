﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MapApp.DATA.Model;

namespace MapApp.DAL.Repository.City
{
    public interface ICountyRepository
    {
        IList<County> GetCounties(string state);
    }
}
