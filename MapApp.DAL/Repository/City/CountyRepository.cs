﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using MapApp.DATA.Model;


namespace MapApp.DAL.Repository.City
{
    public class CountyRepository: ICountyRepository
    {
        public IList<County> GetCounties(string state)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                if(String.IsNullOrWhiteSpace(state))
                    return db.Query<County>("Select DISTINCT(cml.[Property County Name]) as Name FROM Commercial AS cml WHERE cml.[Property County Name] IS NOT null").ToList();
                return db.Query<County>("Select DISTINCT(cml.[Property County Name]) as Name FROM Commercial AS cml WHERE cml.[Property County Name] IS NOT null AND cml.[Property State] = @state", new { state }).ToList();
            }
        }
    }
}
