﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using MapApp.DATA.Model;

namespace MapApp.DATA.Helper
{
    public class GeocodeApiGoogle
    {
        public static PointCoordinate GetCoordinates(string address, string tooltipValue)
        {
            try
            {
                var requestUri = string.Format(
                    "https://maps.googleapis.com/maps/api/geocode/xml?address={0}&sensor=false&key=AIzaSyDJB-jEIsFdx9tWcIzUW6EOgIoI6c409Ao",
                    Uri.EscapeDataString(address));

                var request = WebRequest.Create(requestUri);
                var response = request.GetResponse();
                var xdoc = XDocument.Load(response.GetResponseStream());
                var result = xdoc.Element("GeocodeResponse").Element("result");
                var locationElement = result.Element("geometry").Element("location");
                var lat = locationElement.Element("lat");
                var lng = locationElement.Element("lng");
                return new PointCoordinate()
                {
                    lat = Convert.ToDouble(lat.Value),
                    lng = Convert.ToDouble(lng.Value),
                    tooltip = tooltipValue
                };
            }
            catch (Exception ex)
            {
                return new PointCoordinate()
                {
                    lat = Convert.ToDouble(0.0),
                    lng = Convert.ToDouble(0.0),
                    tooltip = tooltipValue
                };
            }
        }
    }
}
