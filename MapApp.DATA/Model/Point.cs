﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapApp.DATA.Model
{
    public class Point
    {
        public string State { get; set; }
        public string City { get; set; }
        public string FullAddress { get; set; }
        public string LienNumber { get; set; }
        public string Owner { get; set; }
        public string LienHolder { get; set; }
        public string Zip { get; set; }
        public double lat { get; set; }
        public double lng { get; set; }
    }
}
