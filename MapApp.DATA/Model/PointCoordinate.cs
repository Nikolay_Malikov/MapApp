﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapApp.DATA.Model
{
    public class PointCoordinate
    {
        public double lat { get; set; }
        public double lng { get; set; }
        public string tooltip { get; set; }
    }
}
