﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapApp.DATA.Model
{
    public class ClientContact
    {
        public string Company_Name { get; set; }
        public string Website { get; set; }
        public float Revenue { get; set; }
        public string First_Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Phone_Number { get; set; }
        public string Phone_Extention { get; set; }
        public string Email { get; set; }
        public string F11 { get; set; }

    }
}
