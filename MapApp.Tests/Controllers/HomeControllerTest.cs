﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MapApp;
using MapApp.BLL.Contracts;
using MapApp.Controllers;
using MapApp.DATA.Model;
using MapApp.Tests.Common;
using Microsoft.Practices.Unity;
using Moq;

namespace MapApp.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest: UnityContainerTests
    {
        private readonly Mock<ICountyService> _mockCountyService = new Mock<ICountyService>();
        private readonly Mock<IStateService> _mockStateService = new Mock<IStateService>();
        private readonly Mock<IPointService> _mockPointService = new Mock<IPointService>();

        [TestInitialize()]
        public override void InitializeTests()
        {
            UnityContainer = new UnityContainer()
                .RegisterType<HomeController>()
                .RegisterInstance(typeof(ICountyService), _mockCountyService.Object)
                .RegisterInstance(typeof(IPointService), _mockPointService.Object)
                .RegisterInstance(typeof(IStateService), _mockStateService.Object);

            ServiceLocator = new UnityServiceLocator(UnityContainer);
        }

        [TestMethod]
        public void Index()
        {
            //_stateService.GetStates()
            //Arrange
            var states = new List<State>();
            _mockStateService.Setup(x => x.GetStates())
                .Returns(states).Verifiable();
            var controller = ServiceLocator.GetInstance(typeof(HomeController)) as HomeController;

            //Act
            var result = controller.Index() as ViewResult;
            var model = result.Model as List<SelectListItem>;

            Assert.IsNotNull(model);
            Assert.AreEqual(1, model.Count);
            _mockStateService.Verify();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void About()
        {
            // Arrange
            //HomeController controller = new HomeController();

            //// Act
            //ViewResult result = controller.About() as ViewResult;

            //// Assert
            //Assert.AreEqual("Your application description page.", result.ViewBag.Message);
        }

        [TestMethod]
        public void GetCounties()
        {
            var counties = new List<County>();
            _mockCountyService.Setup(x => x.GetCities(It.IsAny<string>()))
                .Returns(counties).Verifiable();
            var controller = ServiceLocator.GetInstance(typeof(HomeController)) as HomeController;

            //Act
            var result = controller.GetCounties("") as JsonResult;
            var model = result.Data as dynamic;

            Assert.IsNotNull(model);
            _mockCountyService.Verify();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetStates()
        {
            var states = new List<State>();
            _mockStateService.Setup(x => x.GetStates())
                .Returns(states).Verifiable();
            var controller = ServiceLocator.GetInstance(typeof(HomeController)) as HomeController;

            //Act
            var result = controller.GetStates() as JsonResult;
            var model = result.Data as dynamic;

            Assert.IsNotNull(model);
            _mockStateService.Verify();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetCenterCoordinate()
        {
            var states = new PointCoordinate();
            var controller = ServiceLocator.GetInstance(typeof(HomeController)) as HomeController;

            //Act
            var result = controller.GetCenterCoordinate("","") as JsonResult;
            var model = result.Data as dynamic;

            Assert.IsNotNull(model);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetPoints()
        {
            var points = new List<Point>();
            _mockPointService.Setup(x => x.GetPoints(It.IsAny<string>(),It.IsAny<string>()))
                .Returns(points).Verifiable();
            var controller = ServiceLocator.GetInstance(typeof(HomeController)) as HomeController;

            //Act
            var result = controller.GetPoints("","") as JsonResult;
            var model = result.Data as object;

            Assert.IsNotNull(model);
            _mockPointService.Verify();
            Assert.IsNotNull(result);
        }
    }
}
