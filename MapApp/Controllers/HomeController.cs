﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Xml.Linq;
using MapApp.BLL.Contracts;
using MapApp.BLL.Services;
using MapApp.DATA.Helper;
using MapApp.DATA.Model;

namespace MapApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICountyService _countyService;
        private readonly IStateService _stateService;
        private readonly IPointService _pointService;
        public HomeController(ICountyService countyService, IStateService stateService, IPointService pointService)
        {
            _countyService = countyService;
            _stateService = stateService;
            _pointService = pointService;
        }

        public ActionResult Index()
        {
            var states = _stateService.GetStates().Select(x => new SelectListItem()
            {
                Text = x.Name,
                Value = x.Name
            }).ToList();
            states.Insert(0,new SelectListItem()
            {
                Value = "",
                Text = "--ALL--",
                Selected = true
            });
            return View(states);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public JsonResult GetCounties(string state)
        {
            try
            {
                var counties = _countyService.GetCities(state);
                return Json(new { obj = counties, success = true});
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = false,
                    message = ex.Message
                });
            }
        }

        [HttpPost]
        public JsonResult GetStates()
        {
            try
            {
                var states = _stateService.GetStates();
                return Json(new { success = true, obj = states});
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult GetCenterCoordinate(string state, string county)
        {
            try
            {
                var address = "";
                if(string.IsNullOrWhiteSpace(state) && string.IsNullOrWhiteSpace(county))
                    address = "USA";
                else
                    address = county + " county, " + state + " state, USA";
                var pointCoordinates = GeocodeApiGoogle.GetCoordinates(address, "Center");
                return Json(new { success = true, obj = pointCoordinates });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message });
            }

        }

        [HttpPost]
        public JsonResult GetPoints(string state, string county)
        {
            try
            {
                var points = _pointService.GetPoints(county, state);
                return Json(new { success = true, obj = points });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message });
            }
        }
    }
}