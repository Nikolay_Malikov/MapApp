﻿var xhr = new window.XMLHttpRequest();
function updateCounties() {
    $.ajax({
        type: "POST",
        url: 'Home/GetCounties',
        data: {
            state: $('#selectPickerState').val()
        },
        success: function(response) {
            if (response.success) {
                var select = document.getElementById("selectPickerCounty");
                var option = document.createElement("option");
                option.selected = "selected";
                option.text = "--ALL--";
                option.value = "";
                select.add(option);
                $.each(response.obj,
                    function(i, item) {
                        var option = document.createElement("option");
                        option.text = item.Name;
                        select.add(option);
                    });
                $("#selectPickerCounty").selectpicker('refresh');
            } else
                swal("Opss!", "Something went wrong. Message:" + response.message, "error");
        }
    });
}

function clearMarkers() {
    setMapOnAll(null);
}

function showMarkers() {
    setMapOnAll(map);
}

function setMapOnAll(googleMap) {
    if (googleMap == null) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(googleMap);
        }
        markerCluster.clearMarkers();
        return;
    }
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
    markerCluster = new MarkerClusterer(googleMap, markers, { imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m' });
}

function onUpdateStateValue(object) {
    event.preventDefault();
    $.ajax({
        type: "POST",
        url: 'Home/GetCounties',
        data: {
            state: object.value
        },
        success: function(response) {
            if (response.success) {
                var select = document.getElementById("selectPickerCounty");
                $("#selectPickerCounty option").remove();
                var option = document.createElement("option");
                option.selected = "selected";
                option.text = "--ALL--";
                option.value = "";
                select.add(option);
                $.each(response.obj,
                    function(i, item) {
                        var option = document.createElement("option");
                        option.text = item.Name;
                        select.add(option);
                    });
                $("#selectPickerCounty").selectpicker('refresh');
                if (xhr != undefined)
                    xhr.abort();
                clearMarkers();
                if ($("#selectPickerState").val() == "" || $("#selectPickerState").val() == null) {
                    map.setZoom(4);
                }
                else
                    map.setZoom(5);
                $('#loading').css('visibility', 'visible');
                $.ajax({
                    type: "POST",
                    url: 'Home/GetPoints',
                    data: {
                        state: $("#selectPickerState").val(),
                        county: ""
                    },
                    xhr : function(){
                        return xhr;
                    },
                    success: function (response) {
                        if (response.success) {
                            markers = response.obj.map(function(location, i) {
                                return new google.maps.Marker({
                                    position: location,
                                    label: location.LienNumber,
                                    city: location.City,
                                    fullAddress: location.FullAddress,
                                    owner: location.Owner,
                                    lienHolder: location.LienHolder,
                                    zip: location.Zip,
                                    state: location.State
                                });
                            });
                            showMarkers();
                            $('#loading').css('visibility', 'hidden');
                        }
                        else
                            swal("Opss!", "Something went wrong. Message:" + response.message, "error");
                    }
                });
            } else
                swal("Opss!", "Something went wrong. Message:" + response.message, "error");
        }
    });
}

function onUpdateCountyValue() {
    event.preventDefault();
    if (xhr != undefined)
        xhr.abort();
    clearMarkers();
    if (($("#selectPickerState").val() == "" || $("#selectPickerState").val() == null) && ($("#selectPickerCounty").val() == null || $("#selectPickerCounty").val() == "")) {
        map.setZoom(4);
    }
    else
        map.setZoom(6);
    $('#loading').css('visibility', 'visible');
    $.ajax({
        type: "POST",
        url: 'Home/GetPoints',
        data: {
            state: $("#selectPickerState").val(),
            county: $("#selectPickerCounty").val()
        },
        xhr : function(){
            return xhr;
        },
        success: function (response) {
            if (response.success) {
                markers = response.obj.map(function(location, i) {
                    return new google.maps.Marker({
                        position: location,
                        label: location.LienNumber,
                        city: location.City,
                        fullAddress: location.FullAddress,
                        owner: location.Owner,
                        lienHolder: location.LienHolder,
                        zip: location.Zip,
                        state: location.State
                    });
                });
                showMarkers();
                $('#loading').css('visibility', 'hidden');
            } else
                swal("Opss!", "Something went wrong. Message:" + response.message, "error");
        }
    });
}

function setCenter() {
    $.ajax({
        type: "POST",
        url: 'Home/GetCenterCoordinate',
        data: {
            state: $("#selectPickerState").val(),
            county: $("#selectPickerCounty").val()
        },
        success: function (response) {
            var myLatlng = new google.maps.LatLng(response.obj.lat, response.obj.lng);
            map.panTo(myLatlng);
        }
    });
}

function dragMap() {
    var bounds = map.getBounds();
    $('#tableBody').html('');
    var i = 0;
    $.each(markers, function (i, item) {
        if (bounds.contains(item.position)) {
            $('#tableBody').append('<tr>' +
                '<td>' + item.city + '</td>' +
                '<td>' + item.fullAddress + '</td>' +
                '<td>' + item.state + '</td>' +
                '<td>' + item.label + '</td>' +
                '<td>' + item.owner + '</td>' +
                '<td>' + item.lienHolder + '</td>' +
                '<td>' + item.zip + '</td>' +
                '</tr>');
        }
        i++;
        if (i == 25)
            return false;
    });
    
}

